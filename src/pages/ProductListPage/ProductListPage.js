import React, { Component } from 'react'
import ProductList from './../../components/ProductList/ProductList'
import ProductItem from './../../components/ProductItem/ProductItem'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import * as actions from './../../actions/index'

class ProductListPage extends Component {
    componentDidMount(){
        this.props.fetchAllProducts();
    }

    render() {
        let {products} = this.props
        return (
            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <Link to="/product/add" className="btn btn-info mb-10 ">Add new product</Link>
                <ProductList>
                    { this.showProducts(products) }
                </ProductList>
            </div>
        );
    }

    showProducts = prds => {
        let result = null
        if(prds.length > 0) {
            result = prds.map((item, index) => {
                return <ProductItem key={index} product={item} index={index} onDelete={ this.onDelete } />
            })
        }
        return result
    }

    onDelete = id => {
        this.props.deleteProduct(id)
    }
}

const mapStateToProps = state => {
    return {
        products: state.products
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchAllProducts: () => {
            dispatch(actions.actFecthProductsRequest())
        },
        deleteProduct: id => {
            dispatch(actions.actDeleteProductRequest(id))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductListPage);
