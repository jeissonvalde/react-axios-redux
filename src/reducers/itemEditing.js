import * as Types from './../contants/ActionTypes'

var initialState = null

const itemEditingReducer = (state = initialState, action) => {
    switch(action.type) {
        case Types.EDIT_PRODUCT:
            state = action.product
            return {...state}

        default:
            return {...state}
    }
}

export default itemEditingReducer